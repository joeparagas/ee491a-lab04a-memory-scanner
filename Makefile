CC     = gcc
CFLAGS = -g -Wall

TARGET = memoryscanner

all: $(TARGET)

memoryscanner: memoryscanner.c
	$(CC) $(CFLAGS) -o $(TARGET) memoryscanner.c 

clean: 
	rm $(TARGET)

test:
	./$(TARGET)
