///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04a - Memory Scanner
///
/// @file memoryscanner.c
/// @version 1.0
//
/// @author Joseph Paragas <joseph60@hawaii.edu>
/// @brief  Lab 04a - Memory Scanner - EE 491F - Spr 2021
/// @date   09_Feb_2021
/// @info   This file contains the functions for the Memory Scanner Lab  
//            
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

/* Prototypes */
char* retrieve_address1(char*);
char* retrieve_address2(char*);
char* retrieve_permissions(char*);
//////////////////////////////////////////////////////////////////////////////

/* Retrieves the first half of the address */
char* retrieve_address1(char* buffer)
{
   int address_length = strlen(buffer);
   char* address = malloc(address_length);
   int buffer_index = 0;
      
   while ((buffer_index < address_length)) 
   {
      /* If we reach a hyphen, we have retrieved the first half of the address and exit the loop*/
      if(buffer[buffer_index] == '-')
      {

         break;

      }

      address[buffer_index] = buffer[buffer_index];
      buffer_index++;
   }

   return address;
   
}

/* Retrieves the second half of the address */
char* retrieve_address2(char* buffer)
{
   int address_length = strlen(buffer);
   char* address = malloc(address_length);
   int address_index = 0;
   int buffer_index = 0;
   int hyphen = 1;


   while (buffer_index < address_length)
   {
      
      /* Resume getting the address where we left off which was the hyphen */
      if(buffer[buffer_index] == '-')
      {
         hyphen = 0;
      }

      /* Exit loop once we get a whitespace */
      if(buffer[buffer_index] == ' ')
      {

         break;

      }

      if (hyphen == 1)
      {

         address[address_index] = buffer[buffer_index];
         address_index++;

      }

      buffer_index++;

   }

   return address;

}

/* Retrieve the permissions */
/* Source of code: https://stackoverflow.com/questions/8812959/how-to-read-linux-file-permission-programmatically-in-c-c/46436636 */
char* retrieve_permissions(char* buffer)
{
   struct stat st;
   char *permissions = malloc(sizeof(char) * 9 + 1);
   if(stat(buffer, &st) == 0)
   {
      mode_t perm = st.st_mode;
      permissions[0] = (perm & S_IRUSR) ? 'r' : '-';
      permissions[1] = (perm & S_IWUSR) ? 'w' : '-';
      permissions[2] = (perm & S_IXUSR) ? 'x' : '-';
      permissions[3] = (perm & S_IRGRP) ? 'r' : '-';
      permissions[4] = (perm & S_IWGRP) ? 'w' : '-';
      permissions[5] = (perm & S_IXGRP) ? 'x' : '-';
      permissions[6] = (perm & S_IROTH) ? 'r' : '-';
      permissions[7] = (perm & S_IWOTH) ? 'w' : '-';
      permissions[8] = (perm & S_IXOTH) ? 'x' : '-';
      permissions[9] = '\0';
      return permissions;
   }

   else
   {
      
      return strerror(errno);

   }
}


int main(int argc, char* argv[])
{
   FILE* file = fopen("/proc/self/maps", "r");
   char buffer[4096];
   char* address1;
   char* address2; 
   char* permissions;

   int counter = 0;
   int buffer_length = 0;

   /* Check if the file exists */
   if (file == NULL)
   {
      printf("Unable to open %s", "/proc/self/maps");
      exit(EXIT_FAILURE);
   }

   /* While the file is loading on the buffer, put the addresses together */
   while (fgets(buffer, sizeof(buffer), file))
   {
      address1 = retrieve_address1(buffer);
      address2 = retrieve_address2(buffer);
      permissions = retrieve_permissions(buffer);   

      const char hex[] = "0x";
      const unsigned int TOTAL_SIZE = sizeof(address1) + sizeof(hex) + 2 * sizeof('\0');
      char* address1_hex = malloc(TOTAL_SIZE);
      char* address2_hex = malloc(TOTAL_SIZE);

      strcpy(address2_hex, hex);
      strcat(address2_hex, address2);
      
      strcpy(address1_hex, hex);
      strcat(address1_hex, address1);

      buffer_length = address2 - address1;

      char* value = (char*)address1;

      for(int i = 0; i < buffer_length; i++)
      {
         char byte = value[i];
         
         if (byte == 'A')
         {
            counter++;
         }
      }

      printf("%s - %s  %s   Number of bytes read [%d] Number of 'A' is [%d]\n", address1_hex, address2_hex, permissions, buffer_length, counter);

   }

   fclose(file);
   return 0;
}

